var mocha = require('mocha');
var chai = require('chai');
var chaiHttp = require('chai-http');
var should = chai.should();

var server = require('../server');

chai.use(chaiHttp);

describe('Test de conectividad', () => {
    it('google funciona', (done) => {
        chai.request('http://www.google.com.mx')
            .get('/')
            .end((error, resp) => {
                //console.log(resp);
                resp.should.have.status(200);
                done();
            })
    })
});

describe('Test de api Raiz', () => {
    it('Raiz Api Contesta', (done) => {
        chai.request('http://localhost:3000')
            .get('/v3')
            .end((error, resp) => {
                //console.log(resp);
                resp.should.have.status(200);
                done();
            })
    });

    it('Raiz Api funciona', (done) => {
        chai.request('http://localhost:3000')
            .get('/v3')
            .end((error, resp) => {
                resp.should.have.status(200);
                resp.body.should.be.a('array');
                done();
            })
    });

    it('Raiz Api Devuelve dos colecciones', (done) => {
        chai.request('http://localhost:3000')
            .get('/v3')
            .end((error, resp) => {
                resp.should.have.status(200);
                resp.body.should.be.a('array');
                resp.body.length.should.be.eql(2);
                done();
            })
    });

    it('Raiz Api Devuelve objetos formados corecamente', (done) => {
        chai.request('http://localhost:3000')
            .get('/v3')
            .end((error, resp) => {
                resp.should.have.status(200);
                resp.body.should.be.a('array');
                resp.body.length.should.be.eql(2);
                for (let index = 0; index < resp.body.length; index++) {
                    const element = resp.body[index];
                    element.should.have.property('recurso');
                    element.should.have.property('url')
                }
                done();
            })
    });
});

describe('Test de api Usuarios', () => {
    it('Api Usuarios Contesta', (done) => {
        chai.request('http://localhost:3000')
            .get('/v3/usuarios')
            .end((error, resp) => {
                //console.log(resp);
                resp.should.have.status(200);
                done();
            })
    });

    it('Api Usuarios Contesta con un array', (done) => {
        chai.request('http://localhost:3000')
            .get('/v3/usuarios')
            .end((error, resp) => {
                //console.log(resp);
                resp.should.have.status(200);
                resp.body.should.be.a('array');
                done();
            })
    });

    it('Api Usuarios Contesta id Correcto', (done) => {
        var idUsuario = "USR508962";
        chai.request('http://localhost:3000')
            .get('/v3/usuarios/' + idUsuario)
            .end((error, resp) => {
                resp.should.have.status(200);
                resp.body.should.be.a('array');
                for (let index = 0; index < resp.body.length; index++) {
                    const element = resp.body[index];
                    resp.body[index].should.have.property('idusuario');
                    resp.body[index].idusuario.should.be.eql(idUsuario);
                }
                done();
            })
    });

});