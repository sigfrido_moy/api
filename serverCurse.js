

var app = express();

console.log("Aqui funcionando");

var movimJSON = require('./movimientosv2.json');
var UsuariosJSON = require('./Usuarios.json');

app.get('/v1/movimientos', function (req, resp) {
    resp.sendfile('movimientosv1.json');
});

app.get('/v2/movimientos', function (req, resp) {
    resp.send(movimJSON);
});

app.get('/v2/movimientos/:id', function (req, resp) {
    console.log(mostrarPropiedades(movimJSON[req.params.id - 1], "Movimiento"));
    //console.log(movimJSON);
    resp.send(movimJSON[req.params.id - 1]);
});

app.get('/v2/movimientosq', function (req, resp) {
    console.log(req.query);
    resp.send(movimJSON[req.params.id - 1]);
});

app.get('/v2/movimientosp/:id/:nombre', function (req, resp) {
    console.log(req.params);
    resp.send(movimJSON[req.params.id - 1]);
});


app.post('/v2/movimientos', function (req, resp) {
    var movimNuevo = req.body;
    // console.log(req.headers.authorization);
    // if (req.headers.authorization != undefined) {
    console.log("Long:" + movimJSON.length);
    movimNuevo.id = movimJSON.length + 1;
    console.log(mostrarPropiedades(movimNuevo, "MovimientoNuevo"));
    movimJSON.push(movimNuevo);
    resp.send('Se ha dado de alta el movimiento');
    // } else {
    //     resp.send('No esta autorizado');
    // }
});

app.put('/v2/movimientos/:id', function (req, resp) {
    var movimCambiar = req.body;
    var actual = movimJSON[req.params.id - 1];
    //console.log("Importe: " + movimCambiar.importe);
    if (movimCambiar.importe != undefined) {
        actual.importe = movimCambiar.importe;
    }
    //console.log("Ciudad: " + movimCambiar.ciudad);
    if (movimCambiar.ciudad != undefined) {
        actual.ciudad = movimCambiar.ciudad;
    }

    resp.send('Actualizacion Exitosa');
});

app.delete("/v2/movimientos/:id", function (req, resp) {
    console.log("Borrando el movimiento: " + req.params.id);
    console.log("que se encuentra en la posicion: " + (req.params.id - 1));
    var actual = movimJSON[req.params.id - 1];

    var movNegativo = {
        "id": movimJSON.length + 1,
        "ciudad": actual.ciudad,
        "importe": actual.importe * (-1),
        "concepto": "Negativo del " + req.params.id
    };
    console.log(mostrarPropiedades(movNegativo, "MovNegativo"));

    movimJSON.push(movNegativo);
    resp.send('Actualizacion Exitosa');
});

app.get('/v2/usuarios/:id', function (req, resp) {
    console.log(mostrarPropiedades(UsuariosJSON[req.params.id - 1], "Usuario"));
    if (req.params.id < 1 || req.params.id > UsuariosJSON.length) {
        resp.status(400).send('El identificador del recurso debe revisarse');
    }
    resp.send(UsuariosJSON[req.params.id - 1]);
});

app.post('/v2/usuarios/login', function (req, resp) {
    var usuarioLog = req.body;
    console.log("-------------" + usuarioLog);
    console.log("-------------" + JSON.stringify(usuarioLog));
    console.log(mostrarPropiedades(usuarioLog, "UsuarioLogin"));
    var resultado = {};
    resultado.resultado = "KO";
    var idUsuario = -1;

    if ((usuarioLog.email != undefined) && !(usuarioLog.email === "") &&
        (usuarioLog.password != undefined) && !(usuarioLog.password === "")) {
        console.log("Datos minimos informados.");
        var bandera = true;
        var indiceArr = 0;

        while (bandera && (indiceArr < UsuariosJSON.length)) {
            //console.log(mostrarPropiedades(UsuariosJSON[indiceArr], "Usuario_" + indiceArr));
            if (UsuariosJSON[indiceArr].email == usuarioLog.email) {
                bandera = false;
                if (UsuariosJSON[indiceArr].password == usuarioLog.password) {
                    idUsuario = UsuariosJSON[indiceArr].id;
                    UsuariosJSON[indiceArr].estado = "online";
                    resultado.resultado = "OK";
                }
            }
            indiceArr++;
        }
    }
    console.log("Res: " + resultado.resultado);
    console.log("Pos: " + idUsuario);

    resp.setHeader("idUsuario", idUsuario);
    resp.setHeader("Content-Type", "application/json");
    if (resultado.resultado != "OK") {
        resp.status(403).send('{"error":"403","descerror":"Usuario password o certificado incorrectos"}')
    }
    resp.send(resultado);
})

app.post('/v2/usuarios/logout', function (req, resp) {
    var usuarioLog = req.body;
    var indicearrUsuario = usuarioLog.id - 1;
    console.log(mostrarPropiedades(usuarioLog, "UsuarioLogin"));
    var resultado = "KO";

    if (usuarioLog.id < 1 || usuarioLog.id > UsuariosJSON.length) {
        resp.status(400).send('El identificador del recurso debe revisarse');
    }

    if ((usuarioLog.id != undefined) && !(usuarioLog.id === "")) {
        console.log("Datos minimos informados.");
        UsuariosJSON[indicearrUsuario].estado = "offline";
        console.log(mostrarPropiedades(UsuariosJSON[indicearrUsuario], "UsuarioLogoff"));
        resultado = "OK";
    }
    resp.send("Resultado: " + resultado);

});

app.get('/v4/', function (req, resp) {
    resp.sendfile('index.pdf');
});

function mostrarPropiedades(objeto, nombreObjeto) {
    var resultado = "";
    for (var i in objeto) {
        if (objeto.hasOwnProperty(i)) {
            if (typeof objeto[i] == 'function') {
                resultado += mostrarPropiedades(objeto, nombreObjeto);
            } else {
                resultado += nombreObjeto + "." + i + " = " + objeto[i] + "\n";
            }
        }
    }
    return resultado;
}