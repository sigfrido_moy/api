

var express = require('express');
var bodyparser = require('body-parser');
var requestJson = require('request-json');

var app = express();

app.use(bodyparser.json());

app.get('/', function (req, resp) {
    resp.send('HOLA API');
});

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// ***********************************************************************************************************//
// ******************************Verision 3 de la Api conectada a MLAB****************************************//
// ***********************************************************************************************************//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////


/**
 * @apiDefine entytySuccessUser
 * @apiSuccess {string} idusuario Id del usuario confirmado
 * @apiSuccess {string} nombre Nombre del usuario a dar de alta.
 * @apiSuccess {string} apellido Apellido del usuario a dar de alta.
 * @apiSuccess {string} email e-mail de contacto con el usuario.
 * @apiSuccess {string} password password del usuario.
 * @apiSuccess {string} pais País de residencia de usuario.
 * @apiSuccess {string} statusUsuario Estatus del usuario actual.
 * @apiSuccess {string} statusonline Estatus de Enlinea del usuario
 * @apiSuccess {Object[]} accesevent Lista de fecha de acceso del usuario
 * @apiSuccess {String} accesevent.entrada Fecha de ingreso al sistema
 * @apiSuccess {String} accesevent.Fecha de Salida del Sistema
 * 
 */

/**
* @apiDefine entytySuccessAccount
* @apiSuccess {string} idCuenta Id de la Cuenta
* @apiSuccess {string} usuario Id del usaurio dueño de la cuenta.
* @apiSuccess {Number} Saldo Saldo de la cuenta.
* 
*/


/**
 * @apiDefine entytySuccessMovements
 * @apiSuccess {Object[]} movimientos Arreglo de Movimientos de la cuenta.
 * @apiSuccess {String} movimientos.idmov Identificador del movimiento en la cuenta
 * @apiSuccess {String} movimientos.fecha Fecha de generacion del movimiento
 * @apiSuccess {String} movimientos.descripcion Descripcion del movimiento
 * @apiSuccess {String} movimientos.importe Importe del movimiento
 * 
 */

//require('request').debug = true;
var clienteMlab;
var urlmlabBase = 'https://api.mlab.com/api/1/databases/techuniversitymxmrg/collections';
var apikey = "apiKey=J1O96JR17KTZ7mWwYOI5qc1hcgguI_Yp";
var arrstrPropUserNew = ['idusuario', 'nombre', 'apellido', 'email', 'password', 'pais', 'statusUsuario'];
var arrstrPropUserChange = ['nombre', 'apellido', 'email', 'password', 'pais', 'statusUsuario'];
var arrstrUserPropForbiddenChange = ['idusuario', 'accesevent']


// ******************************Obtener Colecciones BD Mongo*********************************
// *******************************************************************************************
app.get('/v3', function (req, resp) {
    var strUrlFinal = urlmlabBase + "?" + apikey
    console.log("URL Final GET /v3:= " + strUrlFinal)

    clienteMlab = requestJson.createClient(strUrlFinal);
    clienteMlab.get('', function (error, resM, bodyResp) {
        var collecionesUsuario = [];
        if (!error) {
            for (let index = 0; index < bodyResp.length; index++) {
                const element = bodyResp[index];

                if (element != "system.indexes") {
                    collecionesUsuario.push({ "recurso": element, "url": "/v3/" + element });
                }
            }
            resp.send(collecionesUsuario);
        } else {
            resp.send(error);
        }

    })
});


// *********************************Obtener Lista de Usuarios*********************************
// *******************************************************************************************
app.get('/v3/usuarios', function (req, resp) {
    var strUrlFinal = urlmlabBase + '/usuarios?' + apikey
    console.log("URL Final GET /v3/usuarios:= " + strUrlFinal)

    clienteMlab = requestJson.createClient(strUrlFinal);
    clienteMlab.get('', function (error, resM, bodyResp) {
        if (!error) {
            resp.send(bodyResp);
        } else {
            resp.send(error);
        }
    });
});


// **************************Crear un Usuarios ***********************************************
// *******************************************************************************************
/**
 * @api {post} /usuarios Crear un usuario nuevo.
 * @apiName createUser
 * @apiGroup Users
 *
 * @apiParam {string} idusuario Identificador único del usuario, comenzara con el prefijo USR + 7 números.
 * @apiParam {string} nombre Nombre del usuario a dar de alta.
 * @apiParam {string} apellido Apellido del usuario a dar de alta.
 * @apiParam {string} email e-mail de contacto con el usuario.
 * @apiParam {string} password password del usuario.
 * @apiParam {string} pais País de residencia de usuario.
 *
 * @apiUse entytySuccessUser
 */
app.post('/v3/usuarios', function (req, resp) {

    var strUrlFinal = urlmlabBase + "/usuarios?" + apikey;
    var objCuerpoRequest = req.body;
    console.log("URL Final  POST /v3/usuarios:= " + strUrlFinal);
    var objNewUser = {};

    var objManejoErrores = {};
    objManejoErrores.hasError = false;
    objManejoErrores.PropiedadError = '';

    objNewUser.statusUsuario = "notconfirmed";
    objNewUser.statusonline = "offline";
    objNewUser.accesevent = [];
    console.log("Objeto inicializado: " + JSON.stringify(objNewUser));

    //Se copian los valores permitidos en campos del mismo nombre, otras propiedades no se copiaran.
    for (var i = 0; i < arrstrPropUserNew.length; i += 1) {
        var strNombrePropiedadActual = arrstrPropUserNew[i];
        console.log("Propiedad Actual " + strNombrePropiedadActual);
        if (objCuerpoRequest.hasOwnProperty(strNombrePropiedadActual)) {
            console.log("Propiedad(" + strNombrePropiedadActual + ") Tipo("
                + typeof objCuerpoRequest[strNombrePropiedadActual] + ")");
            objNewUser[strNombrePropiedadActual] = objCuerpoRequest[strNombrePropiedadActual];
        }
        else {
            objManejoErrores.hasError = true;
            objManejoErrores.PropiedadError += strNombrePropiedadActual;
            objManejoErrores.PropiedadError += ', ';
        }
    }

    if (objManejoErrores.hasError) {
        console.log("hubo error: " + JSON.stringify(objManejoErrores));
        resp.setHeader("Content-Type", "application/json");
        var objError = {};
        objError.errCode = "MandatoryFieldNotInformed";
        objError.errDescription = "No se ha encontrado el campo " + objManejoErrores.PropiedadError +
            " el cual es obligatorio, agreguelo a la peticion e intente de nuevo";
        resp.status(400).send(objError);
    }
    else {
        console.log("Objeto Finalizado: " + JSON.stringify(objNewUser));
        clienteMlab = requestJson.createClient(strUrlFinal);
        clienteMlab.post('', objNewUser, function (error, resM, bodyResp) {
            if (!error) {
                resp.send(bodyResp);
            } else {
                resp.send(error);
            }

        });
    }
});

// *************************Consultar un Usuarios en especifico*******************************
// *******************************************************************************************
/** 
 *
 * @api {get} /usuarios/:idusuario Obtener un Usuarios en especifico.
 * @apiName getUser
 * @apiGroup Users
 *
 * @apiParam {string} idusuario identificador del usuario a consultar.
 * 
 * @apiUse entytySuccessUser
 */
app.get('/v3/usuarios/:id', function (req, resp) {

    var strUrlFinal = urlmlabBase + '/usuarios' + '?q={"idusuario":"' + req.params.id + '"}' + '&' + apikey
    console.log("URL Final  GET /v3/usuarios/:id := " + strUrlFinal);
    clienteMlab = requestJson.createClient(strUrlFinal);
    clienteMlab.get('', function (error, resM, bodyResp) {
        if (!error) {
            resp.send(bodyResp);
        } else {
            resp.send(error);
        }
    });
});


// **********************Actualizar un Usuarios en especifico*********************************
// *******************************************************************************************
/**
 * @api {put} /usuarios Actualizar un usuario nuevo.
 * @apiName UpdateUser
 * @apiGroup Users
 *
 * @apiParam {string} [nombre] Nombre del usuario a dar de alta.
 * @apiParam {string} [apellido] Apellido del usuario a dar de alta.
 * @apiParam {string} [email] e-mail de contacto con el usuario.
 * @apiParam {string} [password] password del usuario.
 * @apiParam {string} [pais] País de residencia de usuario.
 * @apiParam {string} [statusUsuario] Estatus del usuario
 * 
 * @apiUse entytySuccessUser
 */
app.put('/v3/usuarios/:id', function (req, resp) {

    clienteMlab = requestJson.createClient(urlmlabBase + "/usuarios");

    var objBodyToSet = {};
    var objToRequest = {};
    var objCuerpoRequest = req.body;

    var objManejoErrores = {};
    objManejoErrores.hasError = false;
    objManejoErrores.PropiedadError = '';

    //Se verifica si se han enviado propiedades no permitidos para actualizar en el cuerpo de la peticion.
    for (var i = 0; i < arrstrUserPropForbiddenChange.length; i += 1) {
        var strNombrePropiedadActual = arrstrUserPropForbiddenChange[i];
        if (objCuerpoRequest.hasOwnProperty(strNombrePropiedadActual)) {
            objManejoErrores.hasError = true;
            objManejoErrores.PropiedadError += strNombrePropiedadActual;
            objManejoErrores.PropiedadError += ', ';
        }
    }

    if (objManejoErrores.hasError) {
        console.log("hubo error: " + JSON.stringify(objManejoErrores));
        resp.setHeader("Content-Type", "application/json");
        var objError = {};
        objError.errCode = "MandatoryFieldNotInformed";
        objError.errDescription = "No se ha encontrado el campo " + objManejoErrores.PropiedadError +
            " el cual es obligatorio, agreguelo a la peticion e intente de nuevo";
        resp.status(400).send(objError);
    }
    else {
        //Se copian los valores permitidos en campos del mismo nombre, otras propiedades no se copiaran.
        for (var i = 0; i < arrstrPropUserChange.length; i += 1) {
            var strNombrePropiedadActual = arrstrPropUserChange[i];
            if (objCuerpoRequest.hasOwnProperty(strNombrePropiedadActual)) {
                console.log("Propiedad(" + strNombrePropiedadActual + ") Tipo(" + typeof objCuerpoRequest[strNombrePropiedadActual] + ")");
                objBodyToSet[strNombrePropiedadActual] = objCuerpoRequest[strNombrePropiedadActual];
            }
        }

        //Se ejecuta la peticion al backend.
        objToRequest["$set"] = objBodyToSet;
        console.log(JSON.stringify(objToRequest));
        clienteMlab.put('?q={"idusuario":"' + req.params.id + '"}' + '&' + apikey,
            objToRequest, function (error, resM, bodyResp) {
                if (!error) {
                    resp.send(bodyResp);
                } else {
                    resp.send(error);
                }
            });
    }

});


// **********************Login de un usaurio en mLab******************************************
// *******************************************************************************************
/**
 * @api {post} /usuarios/login Ejecutar el login de un usuario.
 * @apiName LoginUser
 * @apiGroup Users
 *
 * @apiParam {string} usuario Nombre del usuario a dar de alta.
 * @apiParam {string} password Apellido del usuario a dar de alta.
 * 
 * @apiSuccess {string} resultado OK para cuando el login ha sido correcto.
 * @apiSuccess {string} usuario Id del usuario confirmado.
 */
app.post('/v3/usuarios/login', function (req, resp) {
    var usuarioLog = req.body;
    console.log("/v3/usuarios/login");
    console.log("-------------" + JSON.stringify(usuarioLog));
    var resultado = {};
    resultado.resultado = "KO";
    var idUsuario = -1;
    var respConsulta;

    //Verificando que en la peticion existen los datos minimos
    if ((usuarioLog.usuario != undefined) && !(usuarioLog.usuario === "") &&
        (usuarioLog.password != undefined) && !(usuarioLog.password === "")) {
        console.log("Se ejecuta la consulta contra mLAB");

        clienteMlab = requestJson.createClient(urlmlabBase + "/usuarios");
        var strQueryMlab = '?q={"idusuario":"' + usuarioLog.usuario + '"}&' + apikey;
        console.log("QueryMlab(" + strQueryMlab + ")");
        //Se eje3cuta la consulta
        clienteMlab.get(strQueryMlab, function (error, resM, bodyResp) {

            console.log("BodyResponse: " + JSON.stringify(bodyResp));
            //Solo hay un registro del idusuario buscado se continua
            if (bodyResp.length == 1) {
                //Si conicide el password, se procede a hacer el login.
                if (bodyResp[0].password == usuarioLog.password) {
                    resultado.resultado = "OK";
                    resultado.usuario = bodyResp[0].idusuario;
                    idUsuario = bodyResp[0].idusuario;
                    //Se obtiene la hora de ingreso
                    var today = new Date().toISOString();

                    var arrConjuntoEntradas = bodyResp[0].accesevent;

                    var objaccesEvent = {};
                    objaccesEvent.entrada = today;
                    arrConjuntoEntradas.push(objaccesEvent);
                    var strValorArregloaccesevent = JSON.stringify(arrConjuntoEntradas);
                    console.log("---------\n" + strValorArregloaccesevent + "\n-------");
                    // Para completar el login, se necesita actualizar el estatus online del usuario, y registrar su entrada.

                    var objSet = JSON.parse('{"$set":{"statusonline":"online","accesevent":' + strValorArregloaccesevent + '}}');
                    var strURLCliente = urlmlabBase + '/usuarios?q={ "idusuario": "' + usuarioLog.usuario + '" }&' + apikey;
                    var clienteMlab2 = requestJson.createClient(strURLCliente);
                    clienteMlab2.put('?q={"idusuario":"' + usuarioLog.usuario + '"}' + '&' + apikey,
                        objSet, function (error, resM, bodyResp) {
                            console.log(bodyResp);
                        });
                }
                //Si no coincide, se enviara el KO y un 403(de mas adelante)
            }
            //Si hubo mas de un usuario o 0 el resultado.resultado viene con KO.
            console.log("Res: " + resultado.resultado);
            console.log("idUsuario: " + idUsuario);
            resp.setHeader("idUsuario", idUsuario);
            //resp.setHeader("Access-Control-Allow-Origin", req.getRemoteAddr() + ":" + req.getRemotePort());
            resp.setHeader("Content-Type", "application/json");
            if (resultado.resultado != "OK") {
                resp.status(403).send('{"error":"403","descerror":"Usuario password o certificado incorrectos"}');
            }
            else {
                resp.send(resultado);
            }
        });

    } else {
        //No fueron informados los datos minimos
        var objResp = {};
        objResp.error = "400";
        objResp.descerror = "No se informaron los datos minimos para realizar la operacion";
        resp.status(400).send(objResp);
    }

});

// **********************Logout de un usaurio en mLab******************************************
// *******************************************************************************************
/**
 * @api {post} /usuarios/logout Ejecutar el login de un usuario.
 * @apiName LogoutUser
 * @apiGroup Users
 *
 * @apiParam {string} usuario Nombre del usuario a hacer Logout.
 * 
 */
app.post('/v3/usuarios/logout', function (req, resp) {
    var usuarioLog = req.body;
    console.log("/v3/usuarios/logout");
    console.log("-------------" + JSON.stringify(usuarioLog));

    if ((usuarioLog.usuario != undefined) && !(usuarioLog.usuario === "")) {

        //Consultar el objeto actual del usuario
        clienteMlab = requestJson.createClient(urlmlabBase + "/usuarios");
        var strQueryMlab = '?q={"idusuario":"' + usuarioLog.usuario + '"}&' + apikey;
        console.log("QueryMlab(" + strQueryMlab + ")");
        //Se eje3cuta la consulta
        clienteMlab.get(strQueryMlab, function (error, resM, bodyResp) {
            console.log("BodyResponse: " + JSON.stringify(bodyResp));

            var today = new Date().toISOString();
            if (bodyResp.length > 0) {
                var arrAccesEvent = bodyResp[0].accesevent;
                if (arrAccesEvent.length > 0) {
                    arrAccesEvent[arrAccesEvent.length - 1].salida = today;
                    var objSet = JSON.parse('{"$set":{"statusonline":"offline","accesevent":' + JSON.stringify(arrAccesEvent) + '}}');
                    var strURLCliente = urlmlabBase + '/usuarios?q={ "idusuario": "' + usuarioLog.usuario + '" }&' + apikey;
                    var clienteMlab2 = requestJson.createClient(strURLCliente);
                    clienteMlab2.put('?q={"idusuario":"' + usuarioLog.usuario + '"}' + '&' + apikey,
                        objSet, function (error, resM, bodyResp) {
                            console.log(bodyResp);
                            resp.status(204).send();
                        });
                }
                else {
                    console.log("Usuario sin accesos previos");
                    var objResp = {};
                    objResp.error = "400";
                    objResp.descerror = "No se encontro Login, verifique la peticion.";
                    resp.status(400).send(objResp);
                }
            } else {
                console.log("No se encontraron usuarios.");
                var objResp = {};
                objResp.error = "404";
                objResp.descerror = "No se encontro el usuario enviado";
                resp.status(404).send(objResp);
            }

        });

    } else {
        //No fueron informados los datos minimos
        var objResp = {};
        objResp.error = "400";
        objResp.descerror = "No se informaron los datos minimos para realizar la operacion";
        resp.status(400).send(objResp);
    }

});

// **********************Obtener cuentas de un usuario en especifico**************************
// *******************************************************************************************
/**
 * @api {get} /usuarios/:id/cuentas/ Obtener las cuentas de un usuario.
 * @apiName getAccount
 * @apiGroup Accounts
 *
 * @apiParam {string} id Identificador del usuario para consultar.
 * 
 *  @apiUse entytySuccessAccount
 */
app.get('/v3/usuarios/:id/cuentas/', function (req, resp) {

    var strUrlFinal = urlmlabBase + '/cuentas?' + 'q={"usuario":"' +
        req.params.id + '"}' + '&' + 'f={"movimientos":0,"_id":0}' + '&' + apikey;
    console.log("URL Final  GET /v3/usuarios/:id/cuentas/:= " + strUrlFinal);

    clienteMlab = requestJson.createClient(strUrlFinal);
    clienteMlab.get('', function (error, resM, bodyResp) {
        resp.send(bodyResp);
    });
});

// **********************************Crear cuenta de un usuario*******************************
// *******************************************************************************************
/**
 * @api {post} /usuarios/:id/cuentas/ Crear una cuenta del usuario especifico.
 * @apiName createAccount
 * @apiGroup Accounts
 *
 * @apiParam {string} id Identificador del usuario para consultar.
 * 
 *  @apiUse entytySuccessAccount
 */
app.post('/v3/usuarios/:id/cuentas/', function (req, resp) {

    var strUrlFinal = urlmlabBase + '/cuentas?' + apikey;
    var strUsuario = req.params.id;
    var objResponse = req.body;
    var objRequestFinal = JSON.parse('{"idcuenta" : "' + objResponse.idcuenta + '","usuario" : "' + strUsuario + '","saldo" : 0,"movimientos":[]}');

    //TODO Verificar que se han enviado los datos completos    
    //TODO Verificar que el usuario enviado existe
    //TODO Verificar que no exista una cuenta con el id enviado
    //TODO Evitar enviar el objeto _id a la salida

    clienteMlab = requestJson.createClient(strUrlFinal);
    clienteMlab.post('', objRequestFinal, function (error, resM, bodyResp) {
        if (!error) {
            resp.send(bodyResp);
        } else {
            resp.send(error);
        }
    })

});

// **********************Obtener Movimientos de una cuenta en especifico**********************
// *******************************************************************************************
/**
 * @api {post} /usuarios/:id/cuentas/:idCuenta/movimientos Crear una cuenta del usuario especifico.
 * @apiName createAccount
 * @apiGroup Accounts
 *
 * @apiParam {string} id Identificador del usuario para consultar.
 * @apiParam {string} idCuenta Identificador de la  cuenta para consultar.
 * 
 *  @apiUse entytySuccessMovements
 */
app.get('/v3/usuarios/:id/cuentas/:idCuenta/movimientos', function (req, resp) {

    var strUrlFinal = urlmlabBase + '/cuentas?q={ "usuario": "' + req.params.id +
        '",  "idcuenta": "' + req.params.idCuenta + '"}&f={"movimientos":1,"_id":0}&' + apikey;

    //TODO Verificar que los datos enviados son completos
    //TODO verificar que el usuario existe
    //TODO validar que solo existe una entrada para el usuario enviado
    //TODO Verificar que la cuenta existe
    console.log("URL Final  GET /v3/usuarios/:id/cuentas/:idCuenta/movimientos:= " + strUrlFinal);

    clienteMlab = requestJson.createClient(strUrlFinal);
    clienteMlab.get('', function (error, resM, bodyResp) {

        console.log("Respuesta Nativa---------------\n" + JSON.stringify(bodyResp) + "\n-----------------------")
        if (!error) {
            var objObjetoFinalresponse = {};
            objObjetoFinalresponse.movimientos = [];
            if (JSON.stringify(bodyResp) != "[]") {
                objObjetoFinalresponse.movimientos = bodyResp[0].movimientos;
            }
            resp.send(objObjetoFinalresponse);
        } else {
            resp.send(error);
        }
    });
});

// *******************Dar de alta un Movimiento a una cuenta en especifico********************
// *******************************************************************************************
/**
 * @api {post} /usuarios/:id/cuentas/:idCuenta/movimientos Crear un movimiento a la cuenta del usuario especificado.
 * @apiName createMovements
 * @apiGroup Movements
 *
 * @apiParam {string} id Identificador del usuario para consultar.
 * @apiParam {string} idCuenta Identificador de la  cuenta para consultar.
 * @apiParam {string} fecha Fecha de la creacion del movimiento
 * @apiParam {string} descripcion Descripcion del movimiento
 * @apiParam {Number} importe importe del movimiento
 *   
 */
app.post('/v3/usuarios/:id/cuentas/:idCuenta/movimientos', function (req, resp) {
    //TODO Verifica que los datos enviados esten completos
    //TODO Verificar que la cuenta existe
    //TODO verificar que el usuario existe
    
    var strUrlFinal = urlmlabBase + '/cuentas?q={ "usuario": "' + req.params.id +
        '",  "idcuenta": "' + req.params.idCuenta + '"}&' + apikey;

    var objBodyRequestExterno = req.body;
    clienteMlab = requestJson.createClient(strUrlFinal);

    clienteMlab.get('', function (error, resM, bodyResp) {
        console.log("Respuesta Nativa---------------\n" + JSON.stringify(bodyResp) + "\n-----------------------");
        if (!error) {
            //Verificar si hay exactamente una cuenta de ese cliente y idCuenta
            if (bodyResp.length == 1) {

                var arrobjMovimientosFinales = bodyResp[0].movimientos;
                //Genera el Objeto MovimientoNuevo
                var objMovimientoNuevo = {};
                objMovimientoNuevo.idmov = arrobjMovimientosFinales.length + 1;
                objMovimientoNuevo.fecha = objBodyRequestExterno.fecha;
                objMovimientoNuevo.descripcion = objBodyRequestExterno.descripcion;
                objMovimientoNuevo.importe = objBodyRequestExterno.importe;
                //Agregar el movimiento nuevo al arreglo final para setear
                arrobjMovimientosFinales.push(objMovimientoNuevo);

                var objBodyFinalUpdate = {};
                objBodyFinalUpdate.$set = {};
                objBodyFinalUpdate.$set.movimientos = arrobjMovimientosFinales;

                clienteMlab.put('', objBodyFinalUpdate, function (error, resM, bodyResp) {
                    console.log(bodyResp);
                    resp.status(204).send();
                });

            }
            else if (bodyResp.length == 0) {
                //No se encontro un numero correcto de cuentas de ese usuario, o 0 o >1
                resp.status(404).send(resp);
            }

        } else {
            resp.send(error);
        }
    });
});

// **********************Obtener Una cuenta en especifico de la coleccion de Cuentas**********
// *******************************************************************************************
app.get('/v3/cuentas/:id', function (req, resp) {
    var clienteMlab;
    clienteMlab = requestJson.createClient(urlmlabBase + "/cuentas?");
    var strQueryMlab = 'q={ "usuario": "' + req.params.id + '"}' + '&' + apikey;
    clienteMlab.get(strQueryMlab, function (error, resM, bodyResp) {
        resp.send(bodyResp);
    });
});


app.listen(3000);
console.log("Escuchando en el puerto 3000");